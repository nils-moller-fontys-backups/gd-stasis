﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundsEventCaller : MonoBehaviour
{
    [SerializeField] private CheckBoundsRuntimeSet checkBoundsItems;

    private void OnTriggerExit(Collider other)
    {
        if (checkBoundsItems.Items.Contains(other.gameObject))
        {
            checkBoundsItems.OnLeaveBounds.Invoke(other.gameObject);
        }
    }
}
