﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class RagdollController : MonoBehaviour
{
    [HideInInspector] public bool isRagdoll = false;
    [SerializeField] CinemachineFreeLook FreeLookCam;
    [SerializeField] CinemachineVirtualCamera RagdollCam;

    InputController ic;
    AnimatorController ac;

    private List<Collider> RagdollParts = new List<Collider>();

    Rigidbody rb;
    Collider col;

    private void Awake()
    {
        SetRagDollParts();
    }

    private void Start()
    {
        ic = GetComponent<InputController>();
        ac = GetComponent<AnimatorController>();
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();
    }

    private void SetRagDollParts()
    {
        Collider[] colliders = gameObject.GetComponentsInChildren<Collider>();

        foreach (Collider c in colliders)
        {
            if (c.gameObject != gameObject)
            {
                c.enabled = false;
                RagdollParts.Add(c);
            }
        }
    }

    public void TurnOnRagdoll()
    {
        ic.AllowInput = false;
        rb.useGravity = false;
        rb.velocity = Vector3.zero;
        transform.position += Vector3.up * 5f;

        ac.DisableAnimator();

        RagdollCam.gameObject.SetActive(true);
        FreeLookCam.gameObject.SetActive(false);

        foreach (Collider c in RagdollParts)
        {
            c.enabled = true;
            c.attachedRigidbody.velocity = Vector3.zero;
            c.attachedRigidbody.mass = c.attachedRigidbody.mass / 8;
        }
        col.enabled = false;

        isRagdoll = true;
    }
}
