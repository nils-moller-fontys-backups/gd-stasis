﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorController : MonoBehaviour
{
    [SerializeField] private UnityEventScriptableObject OnStasisActivate;
    Animator animator;

    void Start()
    {
        animator = GetComponentInChildren<Animator>();
        OnStasisActivate.Event.AddListener(PutInStasis);
    }

    public void DisableAnimator()
    {
        animator.enabled = false;
    }
    public void EnableAnimator()
    {
        animator.enabled = true;
    }
    public void Jump()
    {
        animator.SetTrigger("Jump");
    }
    public void Attack()
    {
        animator.SetTrigger("Attack");
    }
    public void SetGrounded(bool isGrounded)
    {
        animator.SetBool("IsGrounded", isGrounded);
    }
    public void SetSpeed(float speed)
    {
        animator.SetFloat("Speed", speed);
    }
    public void PutInStasis()
    {
        animator.SetTrigger("PutInStasis");
    }
}
