﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelSelectMenuController : MonoBehaviour
{
    public void LoadLevel(int buildIndex)
    {
        //to load next level
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

        //to load buildindex scene
        SceneManager.LoadScene(buildIndex);
    }
}
