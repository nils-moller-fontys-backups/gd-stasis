﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UnityGameObjectEvent : UnityEvent<GameObject> { }

public class EnemyCameraVision : MonoBehaviour
{
    public UnityGameObjectEvent OnVisionEnter = new UnityGameObjectEvent();
    public UnityGameObjectEvent OnVisionExit = new UnityGameObjectEvent();

    [SerializeField] private SecurityNetworkData network;
    private Material mat;

    private void Start()
    {
        mat = GetComponent<Renderer>().material;

        network.OnDetectionStateChanged.AddListener(OnDetectionStateChanged);
        OnDetectionStateChanged();
    }

    private void OnDetectionStateChanged()
    {
        mat.SetColor("_Color", network.GetDetectedColor());
    }

    private void OnTriggerEnter(Collider other)
    {
        OnVisionEnter.Invoke(other.gameObject);
    }

    private void OnTriggerExit(Collider other)
    {
        OnVisionExit.Invoke(other.gameObject);
    }
}
