﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTurret : MonoBehaviour
{
    [SerializeField] private GameObject bulletPrefab;
    [SerializeField] private Transform spawnPos;
    [SerializeField] private SecurityNetworkData network;
    private Material detectedMat;
    public float fireInterval;
    private float fireCooldown = 0f;

    private void Start()
    {
        Renderer rend = GetComponent<Renderer>();

        if (rend && rend.materials.Length > 1)
            detectedMat = rend.materials[1];

        fireCooldown = Time.time;

        network.OnDetectionStateChanged.AddListener(OnDetectionStateChanged);
        OnDetectionStateChanged();
    }

    void FixedUpdate()
    {
        if (network.targetTransform != null)
        {
            transform.LookAt(network.targetTransform.position + network.targetTOffset);
            if (Time.time > fireCooldown)
            {
                fireCooldown = Time.time + fireInterval;
                Shoot();
            }
        }
    }

    private void Shoot()
    {
        GameObject obj = Instantiate(bulletPrefab, spawnPos.position, transform.rotation);
        FMODUnity.RuntimeManager.PlayOneShot("event:/Laser", transform.position);
    }

    private void OnDetectionStateChanged()
    {
        if (detectedMat)
            detectedMat.SetColor("_Color", network.GetDetectedColor());
    }
}
