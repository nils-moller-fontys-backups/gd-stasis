﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu]
public class UnityEventScriptableObject : ScriptableObject
{
    public UnityEvent Event = new UnityEvent();
}
