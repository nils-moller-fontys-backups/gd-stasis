﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StasisObjectState
{
    DISABLED,
    VISIBLE,
    HOVERED,
    ACTIVE
}

public class StasisObject : MonoBehaviour
{
    [SerializeField] private StasisObjectRuntimeSet sors;

    private Vector3 stasisDirection;
    private float stasisForce;
    public StasisData data;

    private bool isStasisActive; // Replace with StasisObjectState.ACTIVE (maybe)
    private bool isHovered;
    private StasisObjectState sos;

    private Rigidbody rb;
    private List<Material> mats = new List<Material>();
    private AudioSource audioSource;
    [SerializeField] private GameObject arrowPivot;
    private Material arrowMat;

    [SerializeField] private UnityEventScriptableObject OnStasisEnd;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        arrowMat = arrowPivot.GetComponentInChildren<Renderer>().material;

        Renderer[] rends = GetComponentsInChildren<Renderer>();
        foreach (Renderer r in rends)
            mats.AddRange(r.materials);

        sors.AddItem(this);
        isHovered = false;
        isStasisActive = false;
        ChangeState();
    }

    private void Update()
    {
        ChangeState();
    }

    private void ChangeState()
    {
        StasisObjectState sos = this.sos;
        if (isStasisActive)
            sos = StasisObjectState.ACTIVE;
        else if (data.inStasisView && !isHovered)
            sos = StasisObjectState.VISIBLE;
        else if (data.inStasisView && isHovered)
            sos = StasisObjectState.HOVERED;
        else if (!data.inStasisView)
            sos = StasisObjectState.DISABLED;

        bool changed = sos != this.sos;
        this.sos = sos;

        if (changed)
        {
            foreach(Material mat in mats)
                mat.SetColor("_Emission", data.GetColor(sos));
            if (sos == StasisObjectState.DISABLED)
            {
                foreach (Material mat in mats)
                    mat.SetFloat("_StasisAmount", 0f);
            }
            else
            {
                foreach (Material mat in mats)
                    mat.SetFloat("_StasisAmount", data.stasisNormalAmount);
            }
        }
    }

    public void ToggleStasis()
    {
        SetStasis(!isStasisActive);
    }

    public void SetStasis(bool stasis = true)
    {
        if (stasis && !isStasisActive)
        {
            isStasisActive = true;
            rb.isKinematic = true;
            stasisDirection = Vector3.zero;
            stasisForce = 0f;

            foreach (Material mat in mats)
                mat.SetColor("_Emission", data.GetColor(StasisObjectState.ACTIVE));

            StartCoroutine(StasisTimer());
        }
        else if (!stasis && isStasisActive)
        {
            isStasisActive = false;
            arrowPivot.SetActive(false);
            rb.isKinematic = false;
            rb.velocity = stasisDirection.normalized * stasisForce;
            //TODO: Play sound effect
            OnStasisEnd.Event.Invoke(); //to trigger cooldown
        }
        else return;

        data.inStasisView = !stasis;
    }

    public void AddStasisForce(float force, Vector3 hitPos)
    {
        hitPos.y = transform.position.y - data.fireAngle;
        stasisDirection += (transform.position - hitPos);

        if (!isStasisActive)
        {
            rb.velocity = stasisDirection.normalized * force * data.noStasisForceModifier;
            return;
        }

        stasisForce += force;
        //stasisForce = stasisForce <= data.maxForce ? stasisForce : data.maxForce;

        arrowPivot.transform.LookAt(transform.position + stasisDirection);
        arrowPivot.SetActive(true);

        float chargePercentage = stasisForce / data.maxForce;
        arrowPivot.transform.localScale = new Vector3(1f, 1f, 3f * chargePercentage);
        arrowMat.color = data.GetColor(StasisObjectState.ACTIVE, chargePercentage);

        foreach (Material mat in mats)
            mat.SetColor("_Emission", data.GetColor(StasisObjectState.ACTIVE, chargePercentage));
    }

    public IEnumerator StasisTimer()
    {
        int ticks = 14 + (data.maxActiveTime - 3);

        for (int i = 0; i < ticks; i++)
        {
            float wait = 1f;

            if (i > ticks - 10) // 1 seconds left
            {
                wait = .1f;
            }
            else if (i > ticks - 14) // 2 seconds left
            {
                wait = .25f;
            }
            else if (i > ticks - 16) // 3 seconds left
            {
                wait = .5f;
            }

            yield return new WaitForSeconds(wait);
            if (isStasisActive)
            {
                foreach (Material mat in mats)
                    mat.SetFloat("_StasisAmount", data.stasisFlashAmount);
                FMODUnity.RuntimeManager.PlayOneShot("event:/Ping", transform.position);
                yield return new WaitForSeconds(.1f);
                foreach (Material mat in mats)
                    mat.SetFloat("_StasisAmount", data.stasisNormalAmount);
            }
            else break;
        }

        SetStasis(false);
    }

    public void SetHover(bool hovered)
    {
        isHovered = hovered;
    }
}
