﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "RuntimeSet/StasisObjects")]
public class StasisObjectRuntimeSet : RuntimeSet<StasisObject>
{
    public StasisObject GetByTransform(Transform obj) {
        StasisObject so = obj.GetComponent<StasisObject>();
        if (so != null && items.Contains(so)) // Does transform have a StasisObject component and is it in this list?
            return so;
        else return null;
    }

    public void SetHover(StasisObject sObj = null)
    {
        foreach (StasisObject so in items)
            so.SetHover(false);

        if (sObj != null)
            sObj.SetHover(true);
    }
}
