﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HitForce : MonoBehaviour
{
    [SerializeField] private float requiredForce;
    public UnityEvent OnHitForceReached = new UnityEvent();

    public void OnCollisionEnter(Collision other)
    {
        if (other.rigidbody != null)
        {
            float dot = Vector3.Dot(other.rigidbody.velocity, transform.position - other.transform.position);
            float hitForce = dot * other.rigidbody.mass;
            //float hitForce = other.rigidbody.velocity.magnitude * other.rigidbody.mass;
            if (hitForce > requiredForce)
                OnHitForceReached.Invoke();
            Debug.Log($"Hit {gameObject.name} with force {hitForce}");
        }
    }
}
