﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UnityBoolEvent : UnityEvent<bool> { }

[CreateAssetMenu]
public class StasisData : ScriptableObject
{
    public UnityBoolEvent OnStasisViewChanged = new UnityBoolEvent();

    [SerializeField] [ColorUsage(true, true)] private Color disabledEmission;
    [SerializeField] [ColorUsage(true, true)] private Color visibleEmission;
    [SerializeField] [ColorUsage(true, true)] private Color hoveredEmission;
    [SerializeField] [ColorUsage(true, true)] private Color activeEmission;
    [SerializeField] [ColorUsage(true, true)] private Color chargeEmission;
    public bool inStasisView;
    public float fireAngle;
    public float forcePerHit;
    public float noStasisForceModifier;
    public float maxForce;
    public int maxActiveTime;

    public float stasisFlashAmount = .5f;
    public float stasisNormalAmount = .2f;

    public Color GetColor(StasisObjectState sos, float chargePercentage = 0f)
    {
        if (sos == StasisObjectState.VISIBLE)
            return visibleEmission;
        else if (sos == StasisObjectState.HOVERED)
            return hoveredEmission;
        else if (sos == StasisObjectState.ACTIVE)
            return GetChargeColor(chargePercentage);
        return disabledEmission;
    }

    private Color GetChargeColor(float chargePercentage)
    {
        if (chargePercentage > 0f)
            return Color.Lerp(activeEmission, chargeEmission, chargePercentage);
        return activeEmission;
    }
}
