﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu]
public class SecurityNetworkData : ScriptableObject
{
    [HideInInspector] public float lastDetectedTime;
    [SerializeField] public float timeBeforeUndetected;
    private bool playerDetected;

    public Vector3 targetTOffset;
    [HideInInspector] public Transform targetTransform;
    public string targetTag;
    public LayerMask obstacleMask;

    [SerializeField] [ColorUsage(true, true)] private Color undetectedColor;
    [SerializeField] [ColorUsage(true, true)] private Color detectedColor;

    [HideInInspector] public UnityEvent OnDetectionStateChanged = new UnityEvent();

    private void OnEnable()
    {
        ResetAlarm();
    }

    public bool IsPlayerDetected
    {
        get
        {
            if (!targetTransform)
                playerDetected = false;
            return playerDetected;
        }
        set
        {
            if (value)
                lastDetectedTime = Time.time;
            if (playerDetected != value)
            {
                playerDetected = value;
                OnDetectionStateChanged.Invoke();
            }
        }
    }

    public Color GetDetectedColor()
    {
        if (playerDetected)
            return detectedColor;
        else return undetectedColor;
    }

    public void ResetAlarm()
    {
        IsPlayerDetected = false;
        targetTransform = null;
    }
}
