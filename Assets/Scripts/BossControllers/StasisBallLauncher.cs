﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StasisBallLauncher : MonoBehaviour
{
    [SerializeField] private GameObject spawnObjectPrefab;
    private List<GameObject> activeObjects = new List<GameObject>();
    [SerializeField] private CheckBoundsRuntimeSet checkBoundsItems;
    [SerializeField] private Transform target;
    public float LaunchForce = 50f;
    public float spawnDelay = 5f;

    // Start is called before the first frame update
    void Start()
    {
        checkBoundsItems.OnLeaveBounds.AddListener(OnLeaveBounds);
    }


    private IEnumerator SpawnNewObject()
    {
        while (true)
        {
            yield return new WaitForSeconds(spawnDelay);
            GameObject spawnedObj = Instantiate(spawnObjectPrefab, transform.position/* + (target.position - transform.position).normalized * 5f*/, transform.rotation);
            spawnedObj.GetComponent<Rigidbody>().velocity = (target.position - transform.position).normalized * LaunchForce;
            activeObjects.Add(spawnedObj);
            checkBoundsItems.AddItem(spawnedObj);
        }
    }


    public void StartSpawningBalls()
    {
        StartCoroutine(SpawnNewObject());
    }

    private void OnLeaveBounds(GameObject obj)
    {
        if (activeObjects.Contains(obj))
        {
            Destroy(obj);
        }
    }

}
